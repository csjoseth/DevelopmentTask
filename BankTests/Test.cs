﻿using NUnit.Framework;
using System;
using BankLibary;

namespace BankTests
{
    [TestFixture()]
    public class Test
    {
        Bank bank;
        Person person1;
        Person person2;
        Person person3;
        Money lessThanMin;
        Money minDeposit;

        [SetUp]
        public void BeforeEachTest()
        {
            bank = new Bank();
            person1 = new Person("Ola", "Nordmann");
            person2 = new Person("Kari", "Jensen");
            person3 = new Person("Jørgen", "Klopp");
            lessThanMin = new Money(99);
            minDeposit = new Money(100);
        }

        [Test]
        public void CreateAccount()
        {
            Account account = bank.CreateAccount(person1, minDeposit);
            Assert.That(account, Is.InstanceOf<Account>());
        }

        [Test]
        public void CreateAccountWithInsufficientDeposit()
        {
            Object account = bank.CreateAccount(person1, lessThanMin);
            Assert.That(account, Is.Null);
        }

        [Test]
        public void CheckAccountNumber()
        {
            Account account1 = bank.CreateAccount(person1, minDeposit);
            Account account2 = bank.CreateAccount(person1, minDeposit);
            Account account3 = bank.CreateAccount(person1, minDeposit);
            Account account4 = bank.CreateAccount(person2, minDeposit);

            Assert.That(account1.GetAccountNumber(), Is.EqualTo(1));
            Assert.That(account2.GetAccountNumber(), Is.EqualTo(2));
            Assert.That(account3.GetAccountNumber(), Is.EqualTo(3));
            Assert.That(account4.GetAccountNumber(), Is.EqualTo(1));

        }

        [Test]
        public void GetAccountsForCustomer()
        {
            Account account1 = bank.CreateAccount(person1, minDeposit);
            Account account2 = bank.CreateAccount(person2, minDeposit);
            Account account3 = bank.CreateAccount(person1, minDeposit);
            Account account4 = bank.CreateAccount(person1, minDeposit);
            Account account5 = bank.CreateAccount(person3, minDeposit);

            Account[] accounts = bank.GetAccountsForCustomer(person1);
            Assert.That(accounts[0].GetAccountName(), Is.EqualTo(account1.GetAccountName()));
            Assert.That(accounts[1].GetAccountName(), Is.EqualTo(account3.GetAccountName()));
            Assert.That(accounts[2].GetAccountName(), Is.EqualTo(account4.GetAccountName()));
        }

        [Test]
        public void GetAccountsForAPersonThatIsntCustomer()
        {
			Account account1 = bank.CreateAccount(person1, minDeposit);
			Account account2 = bank.CreateAccount(person2, minDeposit);
			Account account3 = bank.CreateAccount(person1, minDeposit);
			Account account4 = bank.CreateAccount(person1, minDeposit);

            Account[] accounts = bank.GetAccountsForCustomer(person3);
            Assert.That(accounts.Length, Is.EqualTo(0));
        }

        [TestCase(10, 110)]
        [TestCase(50, 150)]
        [TestCase(100.55, 200.55)]
        [TestCase(200, 300)]
        public void DepositToAccount(double amount, double expected)
        {
            Account account1 = bank.CreateAccount(person1, minDeposit);
            bank.Deposit(account1, new Money(amount));
            Assert.That(account1.GetBalance(), Is.EqualTo(expected).Within(0.01));
        }

		[TestCase(10, 90)]
		[TestCase(50, 50)]
		[TestCase(99.9, 0.1)]
        [TestCase(100, 0)]
        public void WithdrawFromAccount(double amount, double expected)
        {
            Account account1 = bank.CreateAccount(person1, minDeposit);
            bank.Withdraw(account1, new Money(amount));
            Assert.That(account1.GetBalance(), Is.EqualTo(expected).Within(0.01));
        }

		[TestCase(101, 100)]
		[TestCase(200, 100)]
        public void WithdrawFromAccountWithToLowBalance(double amount, double expected)
        {
			Account account1 = bank.CreateAccount(person1, minDeposit);
			bank.Withdraw(account1, new Money(amount));
			Assert.That(account1.GetBalance(), Is.EqualTo(expected).Within(0.01));
        }

		[TestCase(50, 50, 150)]
		[TestCase(100, 0, 200)]
        public void TransferFromAccount(double amount, double expectedAccount1, double expectedAccount2)
        {
            Account account1 = bank.CreateAccount(person1, minDeposit);
            Account account2 = bank.CreateAccount(person2, minDeposit);
            bank.Transfer(account1, account2, new Money(amount));
            Assert.That(account1.GetBalance(), Is.EqualTo(expectedAccount1).Within(0.01));
            Assert.That(account2.GetBalance(), Is.EqualTo(expectedAccount2).Within(0.01));
        }

		[TestCase(101, 100, 100)]
		[TestCase(200, 100, 100)]
		public void TransferFromAccountWithToLowBalance(double amount, double expectedAccount1, double expectedAccount2)
		{
			Account account1 = bank.CreateAccount(person1, minDeposit);
			Account account2 = bank.CreateAccount(person2, minDeposit);
			bank.Transfer(account1, account2, new Money(amount));
			Assert.That(account1.GetBalance(), Is.EqualTo(expectedAccount1).Within(0.01));
			Assert.That(account2.GetBalance(), Is.EqualTo(expectedAccount2).Within(0.01));
		}
    }
}