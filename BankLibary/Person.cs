﻿using System;
namespace BankLibary
{
    public class Person
    {
		public string givenName;
		public string surName;
        public string fullName;

		public Person(string givenName, string surName)
		{
			this.givenName = givenName;
			this.surName = surName;
            this.fullName = givenName + surName;
		}
    }
}
