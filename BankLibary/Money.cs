﻿using System;
namespace BankLibary
{
    public class Money
    {

        double Amount;

        public Money(double amount)
        {
            this.Amount = amount;
        }

        public double GetAmount()
        {
            return Amount;
        }
    }
}
