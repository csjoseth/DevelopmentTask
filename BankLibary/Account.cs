﻿using System;
using System.Collections.Generic;

namespace BankLibary
{
    public class Account 
    {
        int accountNumber;
        static List<Person> customers = new List<Person>();
        String accountName;
        Person customer;
        public Money balance;

        public Account(Person customer, Money initialDeposit)
        {

            if (!customers.Contains(customer))
            {
                customers.Add(customer);
                this.accountNumber = 1;
            }
            else
            {
                customers.Add(customer);
                this.accountNumber = customers.Count;
            }

            this.customer = customer;
            this.balance = initialDeposit;
            this.accountName = customer.givenName + customer.surName + accountNumber;
        }

        public Person GetCustomer()
        {
            return customer;
        }

        public int GetAccountNumber()
        {
            return accountNumber;
        }

        public string GetAccountName()
        {
            return accountName;
        }

        public double GetBalance()
        {
            return balance.GetAmount();
        }

        public void SetBalance(Money balance)
        {
            this.balance = balance;
        }
    }
}
