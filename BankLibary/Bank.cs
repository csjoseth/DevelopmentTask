﻿using System;
using System.Collections.Generic;

namespace BankLibary
{
    public class Bank
    {
        static List<Account> accounts = new List<Account>();
        readonly double minStartDeposit = 100;

        public Account CreateAccount(Person customer, Money initialDeposit)
        {
            Account newAccount;

            if (initialDeposit.GetAmount() >= minStartDeposit)
            {
                newAccount = new Account(customer, initialDeposit);
                accounts.Add(newAccount);
            }
            else
            {
                newAccount = null;
            }

            return newAccount;
        }

        public Account[] GetAccountsForCustomer(Person customer)
        {
            Account[] customersAccounts;
            List<Account> result = new List<Account>();

            result = accounts.FindAll(
            delegate (Account a)
            {
                return a.GetCustomer().Equals(customer);
            }
            );
            customersAccounts = result.ToArray();

            return customersAccounts;

        }

        public void Deposit(Account to, Money amount)
        {
			Money newBalance = new Money(to.balance.GetAmount() + amount.GetAmount());
			to.SetBalance(newBalance);
        }

        public void Withdraw(Account from, Money amount)
        {
            if (from.balance.GetAmount() >= amount.GetAmount())
            {
                Money newBalance = new Money(from.balance.GetAmount() - amount.GetAmount());
                from.SetBalance(newBalance);
            }
        }

        public void Transfer(Account from, Account to, Money amount)
        {
			if (from.GetBalance() >= amount.GetAmount())
			{
				Money newBalanceFrom = new Money(from.GetBalance() - amount.GetAmount());
				from.SetBalance(newBalanceFrom);
				Money newBalanceTo = new Money(to.GetBalance() + amount.GetAmount());
				to.SetBalance(newBalanceTo);
			}    
        }
    }

}